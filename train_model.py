from __future__ import print_function
from __future__ import division
import torch
import torch.nn as nn
from layers import SinkhornDistance
import torchvision
import time
import copy
from evaluate import fx_calc_map_label
import torch.nn.functional as F
import numpy as np
import torch.optim as optim
from mmd import mix_rbf_mmd2
import itertools
from torch.autograd import Variable
from utils_PyTorch import show_progressbar

print("PyTorch Version: ", torch.__version__)
print("Torchvision Version: ", torchvision.__version__)


def calc_label_sim(label_1, label_2):
    Sim = label_1.float().mm(label_2.float().t())
    return Sim

def calc_loss(view1_feature, view2_feature, view1_predict, view2_predict, labels_1, labels_2, beta):
    criterion = lambda x, y: (((x - y) ** 2).sum(1).sqrt()).mean()
    term1 = criterion(view1_predict, labels_1) + criterion(view2_predict, labels_2)
    term2 = criterion(view1_feature, view2_feature)

    im_loss = term1 + term2 #+ term3
    return im_loss


def calEMD(dist1, dist2):
    cdf_dist1 = torch.sum(dist1, dim=-1)
    cdf_dist2 = torch.sum(dist2, dim=-1)
    samplewise_emd = torch.sqrt(torch.mean(torch.abs(cdf_dist1 - cdf_dist2), dim=-1))
    return torch.mean(samplewise_emd)

def Semantic_G_loss_img(Se_rec_fake_label_img, Se_rec_label_img, style1, img):
    criterion = lambda x, y: (((x - y) ** 2).sum(1).sqrt()).mean()
    bandwidths = [2.0, 5.0, 10.0, 20.0, 40.0, 80.0]
    concat_Se_rec_fake_label_img = torch.cat(Se_rec_fake_label_img[0:-1],1)
    concat_Se_rec_label_img = torch.cat(Se_rec_label_img[0:-1],1)
    g_loss_img = calEMD(concat_Se_rec_fake_label_img,concat_Se_rec_label_img)
    criterion_1 = lambda x: (((x) ** 2).sum(1).sqrt()).mean()

    return g_loss_img

def Semantic_G_loss_txt(Se_rec_fake_label_txt, Se_rec_label_txt, style2, txt):

    concat_Se_rec_fake_label_txt = torch.cat(Se_rec_fake_label_txt[0:-1],1)
    concat_Se_rec_label_txt = torch.cat(Se_rec_label_txt[0:-1],1)
    g_loss_txt = calEMD(concat_Se_rec_fake_label_txt, concat_Se_rec_label_txt)

    return g_loss_txt


def Semantic_D_loss_img(Se_label_img, Se_fake_img, Se_fake_txt_img):
    #Semantic image discriminator loss
    img_term1 = nn.functional.binary_cross_entropy_with_logits(Se_label_img[-1], torch.ones_like(Se_label_img[-1]))
    img_term2 = nn.functional.binary_cross_entropy_with_logits(Se_fake_img[-1], torch.zeros_like(Se_fake_img[-1]))
    #img_term3 = nn.functional.binary_cross_entropy_with_logits(Se_fake_txt_img[-1], torch.zeros_like(Se_fake_txt_img[-1]))
    d_loss_img = torch.mean(img_term1 + img_term2)
    return d_loss_img

def Semantic_D_loss_txt(Se_label_txt, Se_fake_txt, Se_fake_img_txt):
    #Semantic text discriminator loss
    txt_term1 = nn.functional.binary_cross_entropy_with_logits(Se_label_txt[-1], torch.ones_like(Se_label_txt[-1]))
    txt_term2 = nn.functional.binary_cross_entropy_with_logits(Se_fake_txt[-1], torch.zeros_like(Se_fake_txt[-1]))
    #txt_term3 = nn.functional.binary_cross_entropy_with_logits(Se_fake_img_txt[-1], torch.zeros_like(Se_fake_img_txt[-1]))
    d_loss_txt = torch.mean(txt_term1 + txt_term2)
    return d_loss_txt

def Reconstruct_G_loss_img(rec_cyc_input_img, rec_input_img, input_GAN_img, cyc_input_img, margin):

    concat_rec_cyc_input_img = torch.cat(rec_cyc_input_img[0:-1],1)
    concat_rec_input_img = torch.cat(rec_input_img[0:-1],1)
    g_loss_img = calEMD(concat_rec_cyc_input_img, concat_rec_input_img)

    return g_loss_img

def Reconstruct_G_loss_txt(rec_cyc_input_txt, rec_input_txt, input_GAN_txt, cyc_input_txt, margin):

    concat_rec_cyc_input_txt = torch.cat(rec_cyc_input_txt[0:-1],1)
    concat_rec_input_txt = torch.cat(rec_input_txt[0:-1],1)
    g_loss_txt = calEMD(concat_rec_cyc_input_txt, concat_rec_input_txt)

    return g_loss_txt


def Reconstruct_D_loss_img(Re_fake_img, Re_input_img):
    #Reconstruct image discriminator loss
    img_term1 = nn.functional.binary_cross_entropy_with_logits(Re_fake_img[-1], torch.zeros_like(Re_fake_img[-1]))
    img_term2 = nn.functional.binary_cross_entropy_with_logits(Re_input_img[-1], torch.ones_like(Re_input_img[-1]))
    #img_term3 = nn.functional.binary_cross_entropy_with_logits(ori_fake_img[-1], torch.zeros_like(ori_fake_img[-1]))
    d_loss_img = img_term1 + img_term2
    return d_loss_img

def Reconstruct_D_loss_txt(Re_fake_txt, Re_input_txt):
    #Reconstruct text discriminator loss
    txt_term1 = nn.functional.binary_cross_entropy_with_logits(Re_fake_txt[-1], torch.zeros_like(Re_fake_txt[-1]))
    txt_term2 = nn.functional.binary_cross_entropy_with_logits(Re_input_txt[-1], torch.ones_like(Re_input_txt[-1]))
    #txt_term3 = nn.functional.binary_cross_entropy_with_logits(ori_fake_txt[-1], torch.zeros_like(ori_fake_txt[-1]))
    d_loss_txt = txt_term1 + txt_term2
    return d_loss_txt

def Metric_constraint_dis_loss(true_img, fake_img, true_txt, fake_txt, cate, margin1, margin2):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    criterion = lambda x, y: (((x - y) ** 2).sum(1).sqrt()).mean()
    #true_loss_img = torch.sqrt(torch.sum(torch.norm(true_img-cate).pow(2)))
    true_loss_img = criterion(true_img, cate)
    #I_false_loss_img = torch.sqrt(torch.sum(torch.norm(true_img-fake_img).pow(2)))
    I_false_loss_img = criterion(true_img, fake_img)
    #T_false_loss_img = torch.sqrt(torch.sum(torch.norm(true_img-fake_txt).pow(2)))
    T_false_loss_img = criterion(true_img, fake_txt)

    #true_loss_txt = torch.sqrt(torch.sum(torch.norm(true_txt-cate).pow(2)))
    true_loss_txt = criterion(true_txt, cate)
    #I_false_loss_txt = torch.sqrt(torch.sum(torch.norm(true_txt-fake_img).pow(2)))
    I_false_loss_txt = criterion(true_txt, fake_img)
    #T_false_loss_txt = torch.sqrt(torch.sum(torch.norm(true_txt-fake_txt).pow(2)))
    T_false_loss_txt = criterion(true_txt, fake_txt)
    zeros = torch.zeros(1).to(device)
    dis_loss = torch.max(zeros, (20*true_loss_img-I_false_loss_img-T_false_loss_img))
    #dis_loss1 = torch.max(zeros, (margin1 * true_loss_img - I_false_loss_img + 6)) + torch.max(zeros, (margin2 * true_loss_img - T_false_loss_img + 12))
    dis_loss = dis_loss + torch.max(zeros, (20*true_loss_txt-I_false_loss_txt-T_false_loss_txt))
    #dis_loss2 = torch.max(zeros,(margin1 * true_loss_txt - I_false_loss_txt + 6)) + torch.max(zeros, (margin2 * true_loss_txt - T_false_loss_txt + 12))
    #dis_loss = dis_loss1 + dis_loss2
    #print(true_loss_img)
    #print(I_false_loss_img)
    #print(T_false_loss_img)
    return dis_loss

def deep_coral_loss(generate_img, generate_txt):
    criterion = lambda x, y: (((x - y) ** 2).sum(1).sqrt()).mean()
    # img covariance
    cov_img = torch.mean(generate_img, 0, keepdim=True) - generate_img
    imgs = cov_img.t() @ cov_img
    # txt covariance
    cov_txt = torch.mean(generate_txt, 0, keepdim=True) - generate_txt
    txts = cov_txt.t() @ cov_txt

    loss = criterion(imgs, txts)
    return loss

def compute_covariance(input_data):
    """
    Compute Covariance matrix of the input data
    """
    n = input_data.size(0)  # batch_size

    # Check if using gpu or cpu
    if input_data.is_cuda:
        device = torch.device('cuda:0')
    else:
        device = torch.device('cpu')

    id_row = torch.ones(n).resize(1, n).to(device=device)
    sum_column = torch.mm(id_row, input_data)
    mean_column = torch.div(sum_column, n)
    term_mul_2 = torch.mm(mean_column.t(), mean_column)
    d_t_d = torch.mm(input_data.t(), input_data)
    c = torch.add(d_t_d, (-1 * term_mul_2)) * 1 / (n - 1)

    return c

def get_batch_cate(center_cate, labels):
    batch_cate = []
    #print(type(center_cate))
    for i in range(labels.shape[0]):
        indx = int(labels[i][0]) - 1
        batch_cate.append(center_cate[indx])
    batch_cate = np.array(batch_cate)
    batch_cate = torch.Tensor(batch_cate)
    return batch_cate

def update_center_cate(center_cate, se_img_common, se_txt_common, labels, alpha):
    se_img_common = se_img_common.numpy()
    se_txt_common = se_txt_common.numpy()
    #print(type(se_img_common))
    #print(center_cate.shape)
    for i in range(center_cate.shape[0]):
        se_common = []
        for j in range(labels.shape[0]):
            if labels[j][0] == i+1:
                se_common.append(se_img_common[j])
                se_common.append(se_txt_common[j])
        se_common = np.array(se_common)
        delta_center = center_cate[i]
        for j in range(se_common.shape[0]):
            if j == 0:
                delta_center = center_cate[i] - se_common[j]
            else:
                delta_center = delta_center + (center_cate[i] - se_common[j])

        delta_center = delta_center / (1+se_common.shape[0])
        #print(delta_center.shape)
        center_cate[i] = center_cate[i] - alpha * delta_center
    return center_cate

def train_model(Se_G_img, Se_G_txt, Se_D_img, Se_D_txt, Re_G_img, Re_G_txt, Re_D_img, Re_D_txt, data_loaders, dataset_sizes, alpha, tau, beta, W, center_cate, dataset, seed, num_epochs=500):
    lr = 1e-4
    betas = (0.5, 0.999)
    margin = 20
    #margin2 = 10
    #tau = taus[dataset]
    print("tau: ", tau)
    print("alpha: ", alpha)
    np.random.seed(seed)
    import random as rn
    rn.seed(seed)
    import os
    #os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    Se_G_optm = optim.Adam(itertools.chain(Se_G_img.parameters(), Se_G_txt.parameters(), Re_G_img.parameters(), Re_G_txt.parameters()), lr=lr, betas=betas)
    #sty_optm = optim.Adam(list(Style_gen.parameters()), lr=lr, betas=betas)
    Se_D_optm = optim.Adam(itertools.chain(Se_D_img.parameters(), Se_D_txt.parameters()), lr=lr, betas=betas)

    #Re_G_optm = optim.Adam(itertools.chain(Se_G_img.parameters(), Se_G_txt.parameters()), lr=lr, betas=betas)
    Re_D_optm = optim.Adam(itertools.chain(Re_D_img.parameters(), Re_D_txt.parameters()), lr=lr, betas=betas)

    since = time.time()
    
    test_img_acc_history = []
    test_txt_acc_history = []
    epoch_loss_history =[]
    gan1disc_history = []
    gan2disc_history = []
    gangenerator_history = []

    #best_model_wts = copy.deepcopy(model.state_dict())
    best_model_wts_se_img = copy.deepcopy(Se_G_img.state_dict())
    best_model_wts_se_txt = copy.deepcopy(Se_G_txt.state_dict())
    #best_model_wts_style = copy.deepcopy(Style_gen.state_dict())
    best_acc = 0.0
    criterion = lambda x, y: (((x - y) ** 2).sum(1).sqrt()).mean()

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch+1, num_epochs))
        print('-' * 20)
        valid_loss_min = 0.0
        # Each epoch has a training and validation phase
        for phase in ['train', 'test']:
            num_batch = int(
                np.ceil(
                    dataset_sizes[phase] /
                    data_loaders[phase].batch_size))
            if phase == 'train':
                # Set model to training mode
                #model.train()
                Se_G_img.train()
                Se_D_img.train()
                Se_G_txt.train()
                Se_D_txt.train()
                Re_G_img.train()
                Re_D_img.train()
                Re_G_txt.train()
                Re_D_txt.train()
                #Style_gen.train()
                #Inter_Attention.train()
            else:
                # Set model to evaluate mode
                #model.eval()
                Se_G_img.eval()
                Se_D_img.eval()
                Se_G_txt.eval()
                Se_D_txt.eval()
                Re_G_img.eval()
                Re_D_img.eval()
                Re_G_txt.eval()
                Re_D_txt.eval()
                #Style_gen.eval()
                #Inter_Attention.eval()

            running_loss = 0.0
            gan1disc_loss = 0.0
            gan2disc_loss = 0.0
            gangenerator_loss = 0.0
            running_corrects_img = 0
            running_corrects_txt = 0
            count = 0
            # Iterate over data.
            for i, batch_data in enumerate(data_loaders[phase]):

                imgs, txts, labels, labels_init, cates, imgs_mis, txts_mis = batch_data[0], batch_data[1], batch_data[2], batch_data[3], batch_data[4], batch_data[5], batch_data[6]
                imgs = imgs.float()
                imgs = imgs.float()
                txts = txts.float()

                imgs_mis = imgs_mis.float()
                txts_mis = txts_mis.float()
                labels  = labels.float()
                batch_cate = get_batch_cate(center_cate, labels_init)
                batch_cate = batch_cate.float()

                if torch.sum(imgs != imgs)>1 or torch.sum(txts != txts)>1:
                    print("Data contains Nan.")

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    # Get model outputs and calculate loss
                    # Special case for inception because in training it has an auxiliary output. In train
                    #   mode we calculate the loss by summing the final output and the auxiliary output
                    #   but in testing we only consider the final output.
                    if torch.cuda.is_available():
                        imgs = imgs.to(device)
                        txts = txts.to(device)
                        labels = labels.to(device)
                        batch_cate = batch_cate.to(device)
                        imgs_mis = imgs_mis.to(device)
                        txts_mis = txts_mis.to(device)
                    
                    # zero the parameter gradients
                    Se_D_optm.zero_grad()

                    # update the discriminator
                    se_img_common,_ = Se_G_img(imgs)
                    se_txt_common,_ = Se_G_txt(txts)
                    Se_label_img = Se_D_img(batch_cate)
                    Se_fake_img = Se_D_img(se_img_common)
                    Se_fake_txt_img = Se_D_img(se_txt_common)
                    d_se_loss_img = Semantic_D_loss_img(Se_label_img, Se_fake_img, Se_fake_txt_img)

                    Se_label_txt = Se_D_txt(batch_cate)
                    Se_fake_txt = Se_D_txt(se_txt_common)
                    Se_fake_img_txt = Se_D_txt(se_img_common)
                    d_se_loss_txt = Semantic_D_loss_txt(Se_label_txt, Se_fake_txt, Se_fake_img_txt)

                    d_se_loss = d_se_loss_img + d_se_loss_txt

                    if phase == 'train':
                        d_se_loss.backward()
                        Se_D_optm.step()

                    # zero the parameter gradients
                    Re_D_optm.zero_grad()

                    # get the reconstruct representation

                    se_img_common, style1 = Se_G_img(imgs)
                    se_txt_common, style2 = Se_G_txt(txts)
                    

                    img_to_txt = se_img_common + style2
                    txt_to_img = se_txt_common + style1

                    re_img_ori = Re_G_img(img_to_txt)

                    re_txt_ori = Re_G_txt(txt_to_img)


                    # update the disctiminator

                    Re_fake_img = Re_D_img(re_txt_ori)
                    Re_input_img = Re_D_img(imgs)

                    d_re_loss_img = Reconstruct_D_loss_img(Re_fake_img, Re_input_img)


                    Re_fake_txt = Re_D_txt(re_img_ori)
                    Re_input_txt = Re_D_txt(txts)

                    d_re_loss_txt = Reconstruct_D_loss_txt(Re_fake_txt, Re_input_txt)

                    d_re_loss = d_re_loss_img + d_re_loss_txt

                    if phase == 'train':
                        d_re_loss.backward()
                        Re_D_optm.step()

                    W = torch.tensor(W)
                    W = Variable(W.to(device), requires_grad=False)
                    # zero the parameter gradients
                    Se_G_optm.zero_grad()

                    # update the generators

                    se_img_common, style1 = Se_G_img(imgs)
                    
                    se_txt_common, style2 = Se_G_txt(txts)

                    view1_predict = se_img_common.view([se_img_common.shape[0], -1]).mm(W)
                    view2_predict = se_txt_common.view([se_txt_common.shape[0], -1]).mm(W)

                    se_mis_img, _ = Se_G_img(imgs_mis)
                    se_mis_txt, _ = Se_G_txt(txts_mis)
                    

                    Se_label_img = Se_D_img(batch_cate)
                    Se_fake_img = Se_D_img(se_img_common)
                    Se_fake_txt_img = Se_D_img(se_txt_common)

                    #se_mis_img_fake = Se_D_img(se_mis_img)
                    #se_mis_img_txt = Se_D_img(se_mis_txt)

                    img_preds = view1_predict
                    txt_preds = view2_predict

                    g_se_loss_img = Semantic_G_loss_img(Se_fake_img, Se_label_img, style1, batch_cate).to(device)

                    Se_label_txt = Se_D_txt(batch_cate)
                    Se_fake_txt = Se_D_txt(se_txt_common)
                    Se_fake_img_txt = Se_D_txt(se_img_common)
                    
                    #se_mis_txt_fake = Se_D_txt(se_mis_txt)
                    #se_mis_txt_img = Se_D_txt(se_mis_img)

                    g_se_loss_txt = Semantic_G_loss_txt(Se_fake_txt, Se_label_txt, style2, batch_cate).to(device)

                    #view1_predict, view2_predict = model(se_img_common, se_txt_common)

                    loss = calc_loss(se_img_common, se_txt_common, view1_predict,
                                        view2_predict, labels, labels, beta)
                    
                    coral_loss = deep_coral_loss(se_img_common, se_txt_common)
                    #coral_loss = coral(se_img_common, se_txt_common)
                    #print(coral_loss)
                    #print(loss)
                    #print(g_se_loss_img+g_se_loss_txt)
                    dis_loss = Metric_constraint_dis_loss(se_img_common, se_mis_img, se_txt_common, se_mis_txt, batch_cate, 12, 6)
                    #print(dis_loss)
                    g_se_loss = g_se_loss_img + g_se_loss_txt + dis_loss + loss + coral_loss #0.05 * 
                    #print("g_se_loss: ", g_se_loss)

                    #img_to_txt = torch.cat((se_img_common, style2), 1)
                    img_to_txt = se_img_common + style2
                    txt_to_img = se_txt_common + style1
                    #txt_to_img = torch.cat((se_txt_common, style1), 1)
                    re_img_ori = Re_G_img(img_to_txt)
                    #re_txt_img = Re_G_txt(se_txt_common)
                    re_txt_ori = Re_G_txt(txt_to_img)
                    #mis_img_txt = Re_G_img(se_mis_img)
                    #mis_txt_img = Re_G_txt(se_mis_txt)

                    #Re_fake_img = Re_D_img(re_txt_img)
                    Re_fake_img = Re_D_img(re_txt_ori)
                    Re_input_img = Re_D_img(imgs)

                    g_re_loss_img = Reconstruct_G_loss_img(Re_fake_img, Re_input_img, re_img_ori, imgs, margin).to(device)

                    Re_fake_txt = Re_D_txt(re_img_ori)
                    Re_input_txt = Re_D_txt(txts)

                    g_re_loss_txt = Reconstruct_G_loss_txt(Re_fake_txt, Re_input_txt,re_txt_ori, txts, margin).to(device)

                    g_re_loss = g_re_loss_img + g_re_loss_txt

                    g_loss = g_se_loss + tau * g_re_loss
                    #print(g_loss)

                    if phase == 'train':
                        g_loss.backward()
                        Se_G_optm.step()

                    #update cates
                    if phase == 'train':
                        with torch.no_grad():
                            se_img_common,_= Se_G_img(imgs)
                            se_txt_common,_ = Se_G_txt(txts)

                            se_img_common = se_img_common.cpu()
                            se_txt_common = se_txt_common.cpu()
                            center_cate = update_center_cate(center_cate, se_img_common, se_txt_common, labels_init, alpha)

                # statistics
                running_loss = loss.item()
                gan1disc_loss += d_se_loss.item()
                gan2disc_loss += d_re_loss.item()
                gangenerator_loss += g_loss.item()
                count += 1
                running_corrects_img += torch.sum(torch.argmax(img_preds, dim=1) == torch.argmax(labels, dim=1))
                running_corrects_txt += torch.sum(torch.argmax(txt_preds, dim=1) == torch.argmax(labels, dim=1))
                show_progressbar([i, num_batch], loss=g_loss.item())

            epoch_loss = running_loss #/ len(data_loaders[phase].dataset)
            gan1disc = gan1disc_loss / count
            gan2disc = gan2disc_loss / count
            gangenerator = gangenerator_loss / count

            t_imgs, t_txts, t_labels = [], [], []
            #valid_d_loss = 0.0
            #count_val = 0
            with torch.no_grad():
            #    W = torch.tensor(W)
            #    W = Variable(W.to(device), requires_grad=False)
                for imgs, txts, labels, _, _, _, _ in data_loaders['val']:
                    imgs = imgs.float()
                    txts = txts.float()
                    labels = labels.float()
                    if torch.cuda.is_available():
                        imgs = imgs.to(device)
                        txts = txts.to(device)
                        labels = labels.to(device)
            #        t_view1_feature = Se_G_img(imgs)
            #        t_view2_feature = Se_G_txt(txts)
                    t_view1_feature, _ = Se_G_img(imgs)
                    t_view2_feature, _ = Se_G_txt(txts)

                    t_view1_feature = t_view1_feature.cpu().numpy()
                    t_view2_feature = t_view2_feature.cpu().numpy()
                    labels = labels.cpu().numpy()
                    #print(t_view1_feature.shape)
                    #labels = labels.argmax(1)
                    #temp_it = fx_calc_map_label(t_view1_feature, t_view2_feature, labels)
                    #temp_ti = fx_calc_map_label(t_view2_feature, t_view1_feature, labels)
                    #valid_d_loss = valid_d_loss + (temp_it + temp_ti) / 2.
                    #count_val += 1
            #        t_view1_predict = t_view1_feature.view([t_view1_feature.shape[0], -1]).mm(W)
            #        t_view2_predict = t_view2_feature.view([t_view2_feature.shape[0], -1]).mm(W)
                    #valid_d_loss = valid_d_loss + criterion(t_view1_feature, t_view2_feature)
                    t_imgs.append(t_view1_feature)
                    t_txts.append(t_view2_feature)
                    t_labels.append(labels)
            
            #if count_val != 0:
            #    valid_d_loss = valid_d_loss / count_val
            t_imgs = np.concatenate(t_imgs)
            t_txts = np.concatenate(t_txts)
            t_labels = np.concatenate(t_labels).argmax(1)
            #print(t_labels.shape)
            #aaavalid_d_loss = valid_d_loss.cpu().numpy()
            #if valid_loss_min > valid_d_loss and phase == 'test':
            #    valid_loss_min = valid_d_loss
            #    best_model_wts_se_img = copy.deepcopy(Se_G_img.state_dict())
            #    best_model_wts_se_txt = copy.deepcopy(Se_G_txt.state_dict())

            #t_labels = np.concatenate(t_labels).argmax(1)
            #test_results = utils.multi_test(test_fea, test_lab, self.MAP)
            img2text = fx_calc_map_label(t_imgs, t_txts, t_labels)
            txt2img = fx_calc_map_label(t_txts, t_imgs, t_labels)
            avg = (img2text + txt2img) / 2.
            
            #print('ACC: fx avg {:4f} valid {:4f}'.format(avg, valid_d_loss))
            #if valid_loss_min < valid_d_loss and phase == 'test':
            if phase == 'test' and avg > best_acc:
                #valid_loss_min = valid_d_loss
                best_acc = avg
                #print('Best average ACC: {:4f}'.format(best_acc))
                #if phase == 'test':
                best_model_wts_se_img = copy.deepcopy(Se_G_img.state_dict())
                best_model_wts_se_txt = copy.deepcopy(Se_G_txt.state_dict())
            if phase == 'train':
                gan1disc_history.append(gan1disc)
                gan2disc_history.append(gan2disc)
                gangenerator_history.append(gangenerator)
            print('{} Loss: {:.4f} Best average ACC: {:4f} '.format(phase, gangenerator_loss, best_acc))

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best average ACC: {:4f}'.format(best_acc))

    Se_G_img.load_state_dict(best_model_wts_se_img)
    Se_G_txt.load_state_dict(best_model_wts_se_txt)

    return Se_G_img, Se_G_txt, test_img_acc_history, test_txt_acc_history, gan1disc_history, gan2disc_history, gangenerator_history