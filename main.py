import torch
from datetime import datetime
import scipy.io as sio
import os
from model import Mapping_img, Mapping_txt
from model import Mapping_img_Discriminator, Mapping_txt_Discriminator
from model import Reconstruct_img, Reconstruct_txt
from model import Reconstruct_img_Discriminator, Reconstruct_txt_Discriminator
from train_model import train_model
from load_data import get_loader
import numpy as np
from utils_PyTorch import save_fea_to_txt
from utils_PyTorch import multi_test
from evaluate import fx_calc_map_label
from evaluate import fx_calc_map_data_label

import time
import ruamel_yaml as yaml
os.environ['CUDA_VISIBLE_DEVICES'] = '0'
######################################################################
# Start running

def to_data(x):
    """Converts variable to numpy."""
    try:
        if torch.cuda.is_available():
            x = x.cpu()
        return x.data.numpy()
    except Exception as e:
        return x

def view_result(_acc):
    res = ''
    R = [50, 'ALL']
    for _k in range(len(_acc)):
        res += (' R = ' + str(R[_k]) + ': ')
        res += ((' - mean: %.5f' % (np.sum(_acc[_k]) / (2 * (2 - 1)))) + ' - detail:')
        for _i in range(2):
            for _j in range(2):
                if _i != _j:
                    res += ('%.5f' % _acc[_k][_i, _j]) + ','
    return res 

if __name__ == '__main__':
    
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, default='./configs/pascal_sentence.yaml')
    parser.add_argument('--mode', type=str, default='train')

    args = parser.parse_args()
    config = yaml.load(open(args.config, 'r'), Loader=yaml.Loader)

    mode = args.mode
    dataset = args.dataset
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    
    DATA_DIR = config["dataset"]["root"]
    savePath = config["save_root"]
    

    alpha = config["model"]["alpha"]
    tau = config["model"]["tau"]
    MAX_EPOCH = config["epoch"]
    batch_size = config["batch_size"]
    output_shape = config["output_shape"]
    
    beta = 1e-1
    betas = (0.5, 0.999)
    weight_decay = 0
    
    seed = config["seed"]
    print('seed: ' + str(seed))
    np.random.seed(seed)
    import random as rn
    rn.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    # os.environ['CUDA_VISIBLE_DEVICES'] = '1'
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    from torch.backends import cudnn
    cudnn.enabled = False

    if mode == 'train':
        print('...Data loading is beginning...')

        data_loader, input_data_par, dataset_sizes = get_loader(DATA_DIR, batch_size)

        print('...Data loading is completed...')

        label_num = input_data_par['num_class']
        print(label_num)
        W_FileName = dataset+'_OrthP_300X300.mat'
        try:
            W = sio.loadmat(W_FileName)['W']
            print("Loading the W...")
            W = torch.Tensor(W)        
        except Exception as e:
            print("Building the new orthogonal W...")
            W = torch.Tensor(output_shape, output_shape)
            W = torch.nn.init.orthogonal_(W, gain=1)[:, 0: label_num]
            W = to_data(W)
            sio.savemat(W_FileName, {'W': W})
            

        Se_G_img = Mapping_img(input_dim=input_data_par['img_dim'], label_dim=label_num).to(device)
        Se_G_txt = Mapping_txt(input_dim=input_data_par['text_dim'], label_dim=label_num).to(device)

        Se_D_img = Mapping_img_Discriminator().to(device)
        Se_D_txt = Mapping_txt_Discriminator().to(device)

        Re_G_img = Reconstruct_img(input_dim=300, output_dim=input_data_par['text_dim']).to(device)
        Re_G_txt = Reconstruct_txt(input_dim=300, output_dim=input_data_par['img_dim']).to(device)

        Re_D_img = Reconstruct_img_Discriminator(input_dim=input_data_par['img_dim']).to(device)
        Re_D_txt = Reconstruct_txt_Discriminator(input_dim=input_data_par['text_dim']).to(device)

        center_cate = input_data_par['center_cate']
        
        print('...Training is beginning...')
        # Train and evaluate
        Se_G_img_tf, Se_G_txt_tf, img_acc_hist, txt_acc_hist, gan1disc_history, gan2disc_history, gangenerator_history = train_model(Se_G_img, Se_G_txt, Se_D_img, Se_D_txt, 
                                Re_G_img, Re_G_txt, Re_D_img, Re_D_txt, data_loader, dataset_sizes, alpha, tau, beta, W, center_cate, dataset, seed, MAX_EPOCH)
        print('...Training is completed...')
        print('...Evaluation on testing data...')

        queryImageOutfile = savePath+dataset+"_queryImageFea.txt"
        queryTextOutfile = savePath+dataset+"_queryTextFea.txt"
        dataImageOutfile = savePath+dataset+"_dataImageFea.txt"
        dataTextOutfile = savePath+dataset+"_dataTextFea.txt"

        t_imgs_data, t_txts_data, t_labels_data = [], [], []
        t_imgs_query, t_txts_query, t_labels_query = [], [], []
        test_fea, test_lab = [], []
        #t_style1_query, t_style2_query = [], []
        with torch.no_grad():
            for imgs, txts, labels,_, _, _,_ in data_loader['train']:
                imgs = imgs.float()
                txts = txts.float()
                labels = labels.float()
                if torch.cuda.is_available():
                    imgs = imgs.to(device)
                    txts = txts.to(device)
                    labels = labels.to(device)
                #t_view1_feature = Se_G_img_tf(imgs)
                #t_view2_feature = Se_G_txt_tf(txts)
                t_view1_feature, _ = Se_G_img_tf(imgs)
                t_view2_feature, _ = Se_G_txt_tf(txts)
                #t_view1_feature, t_view2_feature = Inter_Attention(t_view1_feature, t_view2_feature)
                #t_view1_feature, t_view2_feature, _, _ = model(img_common, txt_common)
                t_imgs_data.append(t_view1_feature.cpu().numpy())
                t_txts_data.append(t_view2_feature.cpu().numpy())
                t_labels_data.append(labels.cpu().numpy())

            for imgs, txts, labels,_, _, _, _ in data_loader['test']:
                imgs = imgs.float()
                txts = txts.float()
                labels = labels.float()
                if torch.cuda.is_available():
                    imgs = imgs.to(device)
                    txts = txts.to(device)
                    labels = labels.to(device)

                t_view1_feature, _= Se_G_img_tf(imgs)
                t_view2_feature, _= Se_G_txt_tf(txts)
                t_imgs_query.append(t_view1_feature.cpu().numpy())
                t_txts_query.append(t_view2_feature.cpu().numpy())
                t_labels_query.append(labels.cpu().numpy())


        t_imgs_data = np.concatenate(t_imgs_data)
        t_txts_data = np.concatenate(t_txts_data)
        t_labels_data = np.concatenate(t_labels_data).argmax(1)
        t_imgs_query = np.concatenate(t_imgs_query)
        t_txts_query = np.concatenate(t_txts_query)
        t_labels_query = np.concatenate(t_labels_query).argmax(1)
        #t_style1_query = np.concatenate(t_style1_query)
        #t_style2_query = np.concatenate(t_style2_query)
        test_fea.append(t_imgs_query)
        test_fea.append(t_txts_query)
        test_lab.append(t_labels_query.reshape([-1,]) if min(t_labels_query.shape) == 1 else t_labels_query)
        test_lab.append(t_labels_query.reshape([-1,]) if min(t_labels_query.shape) == 1 else t_labels_query)

        label_data = np.zeros((t_labels_data.shape[0], 1))
        label_query = np.zeros((t_labels_query.shape[0], 1))
        for i in range(t_labels_data.shape[0]):
            label_data[i, :] = t_labels_data[i]
        for i in range(t_labels_query.shape[0]):
            label_query[i, :] = t_labels_query[i]

        save_fea_to_txt(queryImageOutfile,t_imgs_query)
        save_fea_to_txt(queryTextOutfile,t_txts_query)
        save_fea_to_txt(dataImageOutfile,t_imgs_data)
        save_fea_to_txt(dataTextOutfile,t_txts_data)


        test_results = multi_test(test_fea, test_lab, -1)
        print("test resutls:" + view_result(test_results))
        #ret = eng.map(queryImageOutfile,queryTextOutfile,dataImageOutfile,dataTextOutfile, DATA_DIR+"train_img_lab.mat", DATA_DIR+"test_img_lab.mat",nargout=2) 
        #print('MAP:\tI->T:'+str(ret[0])+"\t"+"T->I:"+str(ret[1])) 
        #print('...Average MAP = {}'.format(((ret[0] + ret[1]) / 2.)))
    
    elif mode == 'test':
        import matlab.engine
        eng = matlab.engine.start_matlab()
        print("Only Test: ")
        
        ret = eng.map(config["test"]["query_image"],config["test"]["query_text"],config["test"]["data_image"],config["test"]["data_text"], config["test"]["train_label"], config["test"]["test_label"],nargout=2)
        print('MAP:\tI->T:'+str(ret[0])+"\t"+"T->I:"+str(ret[1])) 
        print('...Average MAP = {}'.format(((ret[0] + ret[1]) / 2.)))
