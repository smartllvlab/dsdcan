import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
import numpy as np

hidden_dim1 = 4096
hidden_dim2 = 2048
input_img_dim = 4096
input_txt_dim = 300

import math

class Mapping_img(nn.Module):
    def __init__(self, input_dim=4096, output_dim=300, label_dim=20, keep_prop=0.9):
        super(Mapping_img, self).__init__()
        self.fc1 = nn.Linear(input_dim, hidden_dim1)
        self.fc2 = nn.Linear(hidden_dim1, hidden_dim1)
        self.fc3 = nn.Linear(hidden_dim1, output_dim)
        
        self.fc4_style = nn.Linear(input_dim, output_dim)

        self.linearLayer = nn.Linear(output_dim, label_dim)
    
    def forward(self, x):
        out = F.relu(self.fc1(x))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)

        style1 = F.relu(self.fc4_style(x))
        #style1 = F.relu(self.fc5_style(style1))
        norm_x = torch.norm(out, dim=1, keepdim=True)
        out = out / norm_x
        return out, style1

class Mapping_txt(nn.Module):
    def __init__(self, input_dim=1000, output_dim=300, label_dim=20, keep_prop=0.9):
        super(Mapping_txt, self).__init__()
        self.fc1 = nn.Linear(input_dim, hidden_dim1)
        self.fc2 = nn.Linear(hidden_dim1, hidden_dim1)
        self.fc3 = nn.Linear(hidden_dim1, output_dim)

        self.fc4_style = nn.Linear(input_dim, output_dim)

        self.linearLayer = nn.Linear(output_dim, label_dim)
    
    def forward(self, x):
        out = F.relu(self.fc1(x))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        
        style2 = F.relu(self.fc4_style(x))
        #style2 = F.relu(self.fc5_style(style2))
        
        norm_x = torch.norm(out, dim=1, keepdim=True)
        out = out / norm_x
        return out, style2

class Mapping_img_Discriminator(nn.Module):
    def __init__(self, input_dim=300, output_dim=1, keep_prop=0.9):
        super(Mapping_img_Discriminator, self).__init__()
        self.fc1 = nn.Linear(input_dim, hidden_dim1)
        self.fc2 = nn.Linear(hidden_dim1, hidden_dim2)
        self.fc3 = nn.Linear(hidden_dim2, output_dim)
    
    def forward(self, x):
        layer1 = F.relu(self.fc1(x))
        layer2 = F.relu(self.fc2(layer1))
        out = self.fc3(layer2)
        return x, layer1, layer2, out

class Mapping_txt_Discriminator(nn.Module):
    def __init__(self, input_dim=300, output_dim=1, keep_prop=0.9):
        super(Mapping_txt_Discriminator, self).__init__()
        self.fc1 = nn.Linear(input_dim, hidden_dim1)
        self.fc2 = nn.Linear(hidden_dim1, hidden_dim2)
        self.fc3 = nn.Linear(hidden_dim2, output_dim)
    
    def forward(self, x):
        layer1 = F.relu(self.fc1(x))
        layer2 = F.relu(self.fc2(layer1))
        out = self.fc3(layer2)
        return x, layer1, layer2, out

class Reconstruct_img(nn.Module):
    def __init__(self, input_dim=300, output_dim=4096,keep_prop=0.9):
        super(Reconstruct_img, self).__init__()
        self.fc3 = nn.Linear(input_dim, output_dim)
    
    def forward(self, x):

        out1 = self.fc3(x)
        norm_x = torch.norm(out1, dim=1, keepdim=True)
        out1 = out1 / norm_x

        return out1

class Reconstruct_txt(nn.Module):
    def __init__(self, input_dim=300, output_dim=1000, keep_prop=0.9):
        super(Reconstruct_txt, self).__init__()
        self.fc3 = nn.Linear(input_dim, output_dim)
    
    def forward(self, x):
        out1 = self.fc3(x)
        norm_x = torch.norm(out1, dim=1, keepdim=True)
        out1 = out1 / norm_x
        return out1


class Reconstruct_img_Discriminator(nn.Module):
    def __init__(self, input_dim=1000, output_dim=1, keep_prop=0.9):
        super(Reconstruct_img_Discriminator, self).__init__()
        self.fc1 = nn.Linear(input_dim, hidden_dim1)
        self.fc2 = nn.Linear(hidden_dim1, hidden_dim2)
        self.fc3 = nn.Linear(hidden_dim2, output_dim)
    
    def forward(self, x):
        layer1 = F.relu(self.fc1(x))
        layer2 = F.relu(self.fc2(layer1))
        out = self.fc3(layer2)
        return x, layer1, layer2, out

class Reconstruct_txt_Discriminator(nn.Module):
    def __init__(self, input_dim=4096, output_dim=1, keep_prop=0.9):
        super(Reconstruct_txt_Discriminator, self).__init__()
        self.fc1 = nn.Linear(input_dim, hidden_dim1)
        self.fc2 = nn.Linear(hidden_dim1, hidden_dim2)
        self.fc3 = nn.Linear(hidden_dim2, output_dim)
    
    def forward(self, x):
        layer1 = F.relu(self.fc1(x))
        layer2 = F.relu(self.fc2(layer1))
        out = self.fc3(layer2)
        return x, layer1, layer2, out
