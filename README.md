# DSDCAN

## The average retrieval result

| dataset | Pascal Sentence | Wikipedia | PKU XMedia |
|:----|:---------|:----------|:----------|
|Query2Gallary| 80.59 | 75.69 | 94.87 |
|Query2Query| 68.35 | 52.04 | 90.75 |

## prepare dataset

Extract the original features for images, texts and category words. The format of dataset is organized as below:
```
## training data
train_img.mat
train_txt.mat
train_img_lab.mat
train_img_cate.mat

## validation data
val_img.mat
val_txt.mat
val_img_lab.mat
val_img_cate.mat

## test data
test_img.mat
test_txt.mat
test_img_lab.mat
test_img_cate.mat
```
Please reset your own path of datasets in the config files.

## Train
```bash
python main.py --config <path-to-config> --mode train
eg. python main.py --config ./configs/pascal_sentence.yaml --mode train
```

## Testing
Only test for Query2Gallery evaluation.
```bash
python main.py --config <path-to-config> --mode test
eg. python main.py --config ./configs/pascal_sentence.yaml --mode test
```