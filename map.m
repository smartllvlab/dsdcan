function [I2Tseen,T2Iseen,rest]= map_zero(qIfile,qTfile,dIfile,dTfile,dataList,queryList)
    qIFea = importdata(qIfile);
    qTFea = importdata(qTfile);
    dIFea = importdata(dIfile);
    dTFea = importdata(dTfile);
    load(dataList);
    load(queryList); 
    Lab_tr_tmp = train_img_lab-1;
    Lab_te_tmp = test_img_lab-1;
    seenDataIndex = find(rem(Lab_tr_tmp,1) == 0);
    seenQueryIndex = find(rem(Lab_te_tmp,1) == 0);
    
    Lab_data_seen = train_img_lab(seenDataIndex,:);
    Lab_query_seen = test_img_lab(seenQueryIndex,:);
    
    
    I_query_seen_re = qIFea(seenQueryIndex,:);
    T_query_seen_re = qTFea(seenQueryIndex,:);
    I_data_seen_re = dIFea(seenDataIndex,:);
    T_data_seen_re = dTFea(seenDataIndex,:);
    
    len_data_seen = size(Lab_data_seen,1);
    len_query_seen = size(Lab_query_seen,1);
    
    I_query_seen_re = znorm(I_query_seen_re);
    T_query_seen_re = znorm(T_query_seen_re);
    I_data_seen_re = znorm(I_data_seen_re);
    T_data_seen_re = znorm(T_data_seen_re);
    
    
    D = pdist([I_query_seen_re; T_data_seen_re],'cosine');
    Z = squareform(D);
    W = 1 - Z;
    W_red = W(1:len_query_seen, len_query_seen+1:end);
    rest = Lab_data_seen;
    [I2Tseen,~] = QryonTestBi(W_red, Lab_query_seen, Lab_data_seen);
    
    D = pdist([T_query_seen_re; I_data_seen_re],'cosine');
    Z = squareform(D);
    W = 1 - Z;
    W_red = W(1:len_query_seen, len_query_seen+1:end);
    [T2Iseen,~] = QryonTestBi(W_red, Lab_query_seen, Lab_data_seen);
    