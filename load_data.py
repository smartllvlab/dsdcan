from torch.utils.data.dataset import Dataset
from scipy.io import loadmat, savemat
from torch.utils.data import DataLoader
import numpy as np
import random

class CustomDataSet(Dataset):
    def __init__(
            self,
            images,
            texts,
            labels,
            labels_init,
            cates,
            img_mis,
            txt_mis):
        self.images = images
        self.texts = texts
        self.labels = labels
        self.labels_init = labels_init
        self.cates = cates
        self.img_mis = img_mis
        self.txt_mis = txt_mis

    def __getitem__(self, index):
        img = self.images[index]
        text = self.texts[index]
        label = self.labels[index]
        label_init = self.labels_init[index]
        cate = self.cates[index]
        img_mis = self.img_mis[index]
        txt_mis = self.txt_mis[index]
        return img, text, label, label_init, cate, img_mis, txt_mis

    def __len__(self):
        count = len(self.images)
        assert len(
            self.images) == len(self.labels)
        return count

#one hot
def ind2vec(ind, N=None):
    ind = np.asarray(ind)
    #print(ind.shape)
    if N is None:
        N = ind.max()
    return np.arange(N) == np.repeat(ind, N, axis=1)

def get_misdate(img_train, text_train, label_train, total_num):
    img_mis_train = []
    txt_mis_train = []
    for x in range(total_num): 
        while(True):
            randTmp = random.randint(0, total_num-1)
            if label_train[randTmp][0] != label_train[x][0]:
                img_mis_train.append(img_train[randTmp])
                txt_mis_train.append(text_train[randTmp])
                break
    
    img_mis_train = np.array(img_mis_train)
    txt_mis_train = np.array(txt_mis_train)
    return img_mis_train, txt_mis_train

def get_center_cate(cate_train, label_train, total_num):
    center_cate = []
    for x in range(total_num):
        temp = 0
        while(True):
            if label_train[temp][0] == x+1:
                center_cate.append(cate_train[temp])
                break
            temp += 1

    center_cate = np.array(center_cate)
    return center_cate

def get_loader(path, batch_size):
    # type: nparray
    img_train = loadmat(path + "train_img.mat")['train_img']
    text_train = loadmat(path + "train_txt.mat")['train_txt']
    label_train_init = loadmat(path + "train_img_lab.mat")['train_img_lab']
    cate_train = loadmat(path +"train_img_cate.mat")['train_img_cate']
    
    text_val = loadmat(path + "val_txt.mat")['val_txt']
    img_val = loadmat(path + "val_img.mat")['val_img']
    label_val_init = loadmat(path + "val_img_lab.mat")['val_img_lab']
    cate_val = loadmat(path + "val_img_cate.mat")['val_img_cate']

    text_test = loadmat(path + "test_txt.mat")['test_txt']
    img_test = loadmat(path + "test_img.mat")['test_img']
    label_test_init = loadmat(path + "test_img_lab.mat")['test_img_lab']
    cate_test = loadmat(path + "test_img_cate.mat")['test_img_cate']

    total = label_train_init.shape[0]
    img_mis_train, txt_mis_train = get_misdate(img_train, text_train, label_train_init, total)
    #print(label_train_init.shape) # pascal(900, 1)
    label_train = ind2vec(label_train_init).astype(int)
    label_test = ind2vec(label_test_init).astype(int)
    label_val = ind2vec(label_val_init).astype(int)   

    img_dim = img_train.shape[1]
    text_dim = text_train.shape[1]
    num_class = label_train.shape[1]

    center_cate = get_center_cate(cate_train, label_train_init, num_class)

    zeros_temp = np.zeros(1)

    imgs = {'train': img_train, 'val': img_val, 'test': img_test}
    texts = {'train': text_train, 'val': text_val, 'test': text_test}
    labels = {'train': label_train, 'val': label_val, 'test': label_test}
    labels_init = {'train': label_train_init, 'val': label_val_init, 'test': label_test_init}
    cates = {'train': cate_train, 'val': cate_val, 'test': cate_test}
    imgs_mis = {'train': img_mis_train, 'val': img_mis_train, 'test': img_mis_train}
    txts_mis = {'train': txt_mis_train, 'val': txt_mis_train, 'test': txt_mis_train}
    dataset = {x: CustomDataSet(images=imgs[x], texts=texts[x], labels=labels[x], labels_init=labels_init[x], cates=cates[x], img_mis=imgs_mis[x], txt_mis=txts_mis[x])
               for x in ['train', 'val', 'test']}

    shuffle = {'train': False, 'val': False, 'test': False}

    dataloader = {x: DataLoader(dataset[x], batch_size=batch_size,
                                shuffle=shuffle[x], num_workers=0) for x in ['train', 'test']}
    dataset_sizes = {x: len(dataset[x]) for x in ['train', 'val', 'test']}

    input_data_par = {}
    input_data_par['img_dim'] = img_dim
    input_data_par['text_dim'] = text_dim
    input_data_par['num_class'] = num_class
    input_data_par['center_cate'] = center_cate
    return dataloader, input_data_par, dataset_sizes
